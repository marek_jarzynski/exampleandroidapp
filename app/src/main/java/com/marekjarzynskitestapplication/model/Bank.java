package com.marekjarzynskitestapplication.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Bank extends RealmObject {
    @PrimaryKey
    @SerializedName("DisKz")
    private String id;
    @SerializedName("DisNameLang")
    private String name;
    @SerializedName("DisPlz")
    private String zip;
    @SerializedName("DisOrt")
    private String city;
    @SerializedName("DisStrasse")
    private String street;
    @SerializedName("DisTel")
    private String tel;
    @SerializedName("DisOeffnung")
    private String openingHours;
    @SerializedName("DisFotoUrl")
    private String imageUrl;
    @SerializedName("DisLatitude")
    private Double latitude;
    @SerializedName("DisLongitude")
    private Double longitude;

    public Bank(String id, String name, String zip, String city, String street, String tel, String openingHours, String imageUrl, Double latitude, Double longitude) {
        this.id = id;
        this.name = name;
        this.zip = zip;
        this.city = city;
        this.street = street;
        this.tel = tel;
        this.openingHours = openingHours;
        this.imageUrl = imageUrl;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Bank() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
