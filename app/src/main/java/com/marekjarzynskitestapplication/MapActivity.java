package com.marekjarzynskitestapplication;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.fragment.app.FragmentActivity;


public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private Bundle bundle;
    private double longitude;
    private double latitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);

        setTitle(R.string.map);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            longitude = bundle.getDouble("longitude", -1.0);
            latitude = bundle.getDouble("latitude", -1.0);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (longitude > -1 && latitude > -1) {
            LatLng location = new LatLng(latitude, longitude);
            googleMap.addMarker(new MarkerOptions().position(location).title("Marker in location"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 16));
        }
    }
}
