package com.marekjarzynskitestapplication;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import com.marekjarzynskitestapplication.adapter.ListAdapter;
import com.marekjarzynskitestapplication.model.Bank;
import com.marekjarzynskitestapplication.retrofit.GetBanksDataService;
import com.marekjarzynskitestapplication.retrofit.RetrofitInstance;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvList;
    private final CompositeDisposable disposable = new CompositeDisposable();
    private RealmResults<Bank> banks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(R.string.list);

        rvList = findViewById(R.id.list);
        rvList.getRecycledViewPool().setMaxRecycledViews(0, 0);
        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(this));
        rvList.setItemAnimator(new DefaultItemAnimator());

        GetBanksDataService banksDataService = RetrofitInstance.getRetrofitInstance().create(GetBanksDataService.class);

        Call<List<Bank>> bankListCall = banksDataService.getBanksData();
        bankListCall.enqueue(new Callback<List<Bank>>() {
            @Override
            public void onResponse(Call<List<Bank>> call, Response<List<Bank>> response) {
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(response.body());
                realm.commitTransaction();
                realm.close();
            }

            @Override
            public void onFailure(Call<List<Bank>> call, Throwable t) {

            }
        });

        Realm realm = Realm.getDefaultInstance();
        banks = realm.where(Bank.class).findAll().sort("zip", Sort.ASCENDING);
        if (banks != null) {
            disposable.add(banks.asFlowable().subscribe(result -> {
                try {
                    banks = result;
                    if (rvList != null) {
                        rvList.getRecycledViewPool().clear();
                        if (rvList.getAdapter() != null) {
                            rvList.getAdapter().notifyDataSetChanged();
                        }
                    }
                } catch (Exception e) {
                    Log.d("error", e.getLocalizedMessage());
                }
            }));
        }

        ListAdapter listAdapter = new ListAdapter(banks, true);
        rvList.setAdapter(listAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
