package com.marekjarzynskitestapplication.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marekjarzynskitestapplication.DetailsActivity;
import com.marekjarzynskitestapplication.R;
import com.marekjarzynskitestapplication.model.Bank;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class ListAdapter extends RealmRecyclerViewAdapter<Bank, ListAdapter.BankViewHolder> {

    class BankViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final AppCompatTextView name;
        final AppCompatTextView zip;
        final AppCompatTextView city;
        final AppCompatTextView street;

        BankViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.tv_name);
            zip = v.findViewById(R.id.tv_zip);
            city = v.findViewById(R.id.tv_city);
            street = v.findViewById(R.id.tv_street);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.isPressed()) {
                int position = getAdapterPosition();
                if (position > -1) {
                    OrderedRealmCollection<Bank> banks = getData();
                    if (banks != null) {
                        Bank bank = banks.get(position);
                        Intent i = new Intent(v.getContext(), DetailsActivity.class);
                        i.putExtra("id", bank.getId());
                        v.getContext().startActivity(i);
                    }
                }
            }
        }
    }

    public ListAdapter(@Nullable OrderedRealmCollection<Bank> data, boolean autoUpdate) {
        super(data, autoUpdate);
    }

    @NonNull
    @Override
    public BankViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bank_list_row, parent, false);
        return new BankViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final BankViewHolder holder, int position) {
        if (position > -1) {
            try {
                OrderedRealmCollection<Bank> banks = getData();
                if (banks != null) {
                    final Bank bank = banks.get(position);
                    holder.name.setText(bank.getName());
                    holder.zip.setText(bank.getZip());
                    holder.city.setText(bank.getCity());
                    holder.street.setText(bank.getStreet());
                }
            } catch (Exception e) {
                Log.d("error", e.getLocalizedMessage());
            }
        }
    }
}
