package com.marekjarzynskitestapplication;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

import com.marekjarzynskitestapplication.model.Bank;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import io.realm.Realm;

public class DetailsActivity extends AppCompatActivity {

    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = (int) (size.x * 0.8);

        setTitle(R.string.details);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            String id = bundle.getString("id");
            Realm realm = Realm.getDefaultInstance();
            final Bank bank = realm.where(Bank.class).equalTo("id", id).findFirst();
            if (bank != null) {
                ((AppCompatTextView) findViewById(R.id.tv_name)).setText(bank.getName());
                Picasso.get()
                        .load(bank.getImageUrl())
                        .resize(width, width).centerInside()
                        .into((AppCompatImageView) findViewById(R.id.iv_image));
                ((AppCompatTextView) findViewById(R.id.tv_zip)).setText(bank.getZip());
                ((AppCompatTextView) findViewById(R.id.tv_city)).setText(bank.getCity());
                ((AppCompatTextView) findViewById(R.id.tv_street)).setText(bank.getStreet());
                ((AppCompatTextView) findViewById(R.id.tv_hours)).setText(bank.getOpeningHours());
                ((AppCompatTextView) findViewById(R.id.tv_phone)).setText(bank.getTel());

                final double latitude = bank.getLatitude();
                final double longitude = bank.getLongitude();

                AppCompatButton buttonMap = findViewById(R.id.btn_map);
                buttonMap.setOnClickListener(v -> {
                    Intent i = new Intent(v.getContext(), MapActivity.class);
                    i.putExtra("latitude", latitude);
                    i.putExtra("longitude", longitude);
                    v.getContext().startActivity(i);
                });
            }
        }
    }
}
