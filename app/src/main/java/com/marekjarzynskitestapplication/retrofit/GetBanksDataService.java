package com.marekjarzynskitestapplication.retrofit;

import com.marekjarzynskitestapplication.model.Bank;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetBanksDataService {
    @GET("/Finanzamtsliste.json")
    Call<List<Bank>> getBanksData();
}
